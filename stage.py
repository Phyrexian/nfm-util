import math

class StagePart:
    def __init__(self, pref, kind, x, z, xz, suf, y=250):
        self.pref = pref
        self.kind = kind
        self.x = x
        self.z = z
        self.xz = xz
        self.suf = suf
        self.y = y

    def __repr__(self):
        x, y, z = (math.floor(d) for d in (self.x, self.y, self.z))
        if self.pref == 'fix':
            return f'{self.pref}({self.kind},{x},{z},{y},{self.xz}){self.suf}'
        return f'{self.pref}({self.kind},{x},{z},{self.xz}){self.suf}'

    def translate(self, x, z):
        self.x += x
        self.z += z

    # rotates piece around origin
    def rotate(self, xz):
        self.xz += xz
        xz = math.radians(xz)
        self.x, self.z = self.x * math.cos(xz) - self.z * math.sin(xz), self.x * math.sin(xz) + self.z * math.cos(xz)

    @staticmethod
    def from_line(line):
        try:
            l = line.index('(')
            r = line.index(')')
            pref = line[0:l]
            vals = [int(x) for x in line[l + 1:r].split(',')]
            suf = line[r + 1:]
            if pref in ['set', 'chk']:
                return StagePart(pref, vals[0], vals[1], vals[2], vals[3], suf)
            if pref == 'fix':
                return StagePart(pref, vals[0], vals[1], vals[2], vals[4], suf, vals[3])
            return None
        except:
            return None

class Stage:
    def __init__(self):
        self.parts = []

    def from_input(self):
        try:
            while True:
                part = StagePart.from_line(input())
                if part != None:
                    self.parts.append(part)
        except:
            pass

    def translate(self, x, z):
        for part in self.parts:
            part.translate(x, z)
    
    def rotate(self, xz):
        for part in self.parts
            part.rotate(xz)

    def fit_walls(self, rlx=0, btx=0):
        min_x = 0
        max_x = 0
        min_z = 0
        max_z = 0
        for part in self.parts:
            min_x = min(min_x, part.x)
            max_x = max(max_x, part.x)
            min_z = min(min_z, part.z)
            max_z = max(max_z, part.z)

        center_x = math.floor((max_x + min_x) / 2)
        center_z = math.floor((max_z + min_z) / 2)
        rlc = math.ceil((max_z - min_z) / 4800) + rlx
        btc = math.ceil((max_x - min_x) / 4800) + btx
        lp = center_x - btc * 2400
        bp = center_z - rlc * 2400
        rp = center_x + btc * 2400
        tp = center_z + rlc * 2400
        rls = bp + 2400
        bts = lp + 2400
        print(f'maxl({rlc},{lp},{rls})')
        print(f'maxb({btc},{bp},{bts})')
        print(f'maxr({rlc},{rp},{rls})')
        print(f'maxt({btc},{tp},{bts})')